
package com.mycompany.httpclientexample;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class HttpClientExample {


    public static void main(String[] args) throws IOException {
       System.out.println("Hello from HttpClientExample");
    
      //Creating a HttpClient object - Postman
      CloseableHttpClient httpclient = HttpClients.createDefault();
      // HttpGet httpget = new HttpGet("https://api.openweathermap.org/data/2.5/weather?q=London&appid=3ea4abe0212f835881380b8afaa025d4");
      // HttpGet httpget = new HttpGet("https://petstore.swagger.io/v2/pet/findByStatus?status=available");
      // HttpGet httpget = new HttpGet("https://jsonplaceholder.typicode.com/users/2/todos");
      HttpGet httpget = new HttpGet("https://2af6ab81-0049-4fdc-a2ab-d642a0ceee06.mock.pstmn.io/users/2/todos");
      
      //Printing the method used
      System.out.println("Request Type: "+httpget.getMethod());

      //Executing the Get request
      HttpResponse httpresponse = httpclient.execute(httpget);
      Scanner sc = new Scanner(httpresponse.getEntity().getContent());

      //Printing the status line
      System.out.println(httpresponse.getStatusLine());
      String result = "";
      while(sc.hasNext()) {
         String line = sc.nextLine();
         //System.out.println(line);
         result+=line;
      }
      // System.out.println("data:");
      System.out.println(result);
      String expected = "[  {    \"userId\": 2,    \"id\": 21,    \"title\": \"suscipit repellat esse quibusdam voluptatem incidunt\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 22,    \"title\": \"distinctio vitae autem nihil ut molestias quo\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 23,    \"title\": \"et itaque necessitatibus maxime molestiae qui quas velit\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 24,    \"title\": \"adipisci non ad dicta qui amet quaerat doloribus ea\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 25,    \"title\": \"voluptas quo tenetur perspiciatis explicabo natus\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 26,    \"title\": \"aliquam aut quasi\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 27,    \"title\": \"veritatis pariatur delectus\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 28,    \"title\": \"nesciunt totam sit blanditiis sit\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 29,    \"title\": \"laborum aut in quam\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 30,    \"title\": \"nemo perspiciatis repellat ut dolor libero commodi blanditiis omnis\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 31,    \"title\": \"repudiandae totam in est sint facere fuga\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 32,    \"title\": \"earum doloribus ea doloremque quis\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 33,    \"title\": \"sint sit aut vero\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 34,    \"title\": \"porro aut necessitatibus eaque distinctio\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 35,    \"title\": \"repellendus veritatis molestias dicta incidunt\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 36,    \"title\": \"excepturi deleniti adipisci voluptatem et neque optio illum ad\",    \"completed\": true  },  {    \"userId\": 2,    \"id\": 37,    \"title\": \"sunt cum tempora\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 38,    \"title\": \"totam quia non\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 39,    \"title\": \"doloremque quibusdam asperiores libero corrupti illum qui omnis\",    \"completed\": false  },  {    \"userId\": 2,    \"id\": 40,    \"title\": \"totam atque quo nesciunt\",    \"completed\": true  }]\n";
     System.out.println(expected);
     if (result.equalsIgnoreCase(expected))
     //if (result == expected)
     {
         System.out.println("server response is pass");
     }else
     {
         System.out.println("server response is fail");
     }
      
    }
    
}
