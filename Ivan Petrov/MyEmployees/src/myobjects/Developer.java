package myobjects;

public class Developer implements IEmployee {

    private double salary;
    private double rate;
    private double hours;
    public Developer()
    {
        this.hours = 160;
        this.rate = 1000 / this.hours;
        this.calcSalary();
    }
    public Developer(double hh, double rate)
    {
        this.hours = hh;
        this.rate = rate;
    }
    
    @Override
    public double calcSalary() {
        this.salary = this.hours * this.rate;
        return this.salary;
    }

    @Override
    public String getTitle() {
        return "Developer";
    }
     @Override
    public double calcTax() {
        return this.calcSalary() * 0.065;//6.5%
    }
}

