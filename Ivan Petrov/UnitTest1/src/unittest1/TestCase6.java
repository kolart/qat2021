/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittest1;

import myobjects.*;

public class TestCase6 extends TestCase{

    public TestCase6()
    {
        super();
        this.className = "Middle Developer";
        this.methodName = "calcTax";
    }
    @Override
    protected boolean test() {
        double tax = this.empl.calcTax();
        return tax == ( 160 * 16.5 ) * 0.065;
    }

    @Override
    protected void setUp() {
        this.empl = new Developer(160, 16.5);
    }

//    @Override
//    public String logResult() {
//         if (this.test())
//            return "test_calcTax_for_Middle_Developer_pass";
//        else
//            return "test_calcTax_for_Middle_Developer_fail";
//    }
    
}
