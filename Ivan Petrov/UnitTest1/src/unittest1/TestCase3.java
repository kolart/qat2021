package unittest1;

import myobjects.*;

public class TestCase3  extends TestCase {
    IEmployee empl;
    public TestCase3()
    {
        this.setUp();
                this.className = "Developer";

        this.methodName = "calcTax";
    }
    protected void setUp()
    {
       this.empl = new Developer();
    }

    protected boolean test()
    {
        double tax = this.empl.calcTax();
        return tax == 65;
    }
}
