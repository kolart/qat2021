
package unittest1;
import myobjects.IEmployee;

public class TestCase 
{
    protected IEmployee empl;
    public String methodName;
    public String className;
    public TestCase()
    {
        this.setUp();
    }
    protected void setUp()
    {
        // this.empl = new Cleaner(1000);
    }
    public String logResult()
    {
        //test_имя_метода_имя_Класса_результат
        String logString = "test_"+this.methodName+"_"+this.className;
        if (this.test())
            return logString+"_pass";
        else
            return logString+"_fail";
    }
    protected boolean test()
    {
        return true;
    }
}
