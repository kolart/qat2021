package unittest1;

import myobjects.*;

public class TestCase4  extends TestCase  {
    IEmployee empl;
     public TestCase4()
    {
        this.setUp();
                this.className = "Cleaner";

        this.methodName = "calcTax";
    }
     @Override
    protected void setUp()
    {
        this.empl = new Cleaner(1000);
    }
 
    @Override
    protected boolean test()
    {
        double tax = this.empl.calcTax();
        return tax == 65;
    }
}
