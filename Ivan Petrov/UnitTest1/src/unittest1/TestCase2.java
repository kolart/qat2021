
package unittest1;

import myobjects.*;

public class TestCase2  extends TestCase {
    IEmployee empl;
    public TestCase2()
    {
        this.setUp();
        this.className = "Developer";
        this.methodName = "calcSalary";
    }
    @Override
    protected void setUp()
    {
        this.empl = new Developer();
    }

    @Override
    protected boolean test()
    {
        double result = empl.calcSalary();
        return result == 1000;
    }
}
