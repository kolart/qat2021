
package unittest1;

import myobjects.*;

public class TestCase5 extends TestCase {
    IEmployee empl;
    public TestCase5()
    {
        this.setUp();
     
        this.methodName = "calcSalary";
        //
        this.className = "Middle Developer";

    }
    @Override
    protected void setUp()
    {
        this.empl = new Developer(160, 16.5);
    }
   
    @Override
    protected boolean test()
    {
        double result = empl.calcSalary();
        return result == 160 * 16.5;
    }
}
