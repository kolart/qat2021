
package unittest1;

import myobjects.Cleaner;

public class TestCase1 extends TestCase{
    
    public TestCase1()
    {
        this.setUp();
        this.className = "Cleaner";
        this.methodName = "calcSalary";
    }
    @Override
    protected void setUp()
    {
        this.empl = new Cleaner(1000);
    }

    @Override
    protected boolean test()
    {
        double result = empl.calcSalary();
        return result == 1000;
    }
}
