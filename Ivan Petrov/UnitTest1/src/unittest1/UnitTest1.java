package unittest1;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

import java.io.PrintWriter;//вывод данных (куда угодно)
import java.util.logging.Level;
import java.util.logging.Logger;


public class UnitTest1 {

   
    public static void main(String[] args) {
     
            //1. Работем с интерфейсами -- здесь поддержка от программистов == поддержка архитектуры
            //1.1 Объект тестировани должен быть внутри общего-разделяемого модуля =
            //в библиотеке
            
            //тесты на структуру: классов, моделей, протоколов
            //тесты на структуру требуют работы с механизмом рефлексии
            //и позволяют понять, что программисты внесли изменения в иерархию наследования
            //или переименовали методы
            
            //тесты на логику (test cases)
            //2. тесты должны быть независимыми
            //test 1 = calaSalary() foe Programmer
            //test 2 = calcSalar() for Cleaner
            //test 3 = calcSalay() for Manger
            //2.последовательность вызовов можно менять
            //3.результат прогнозируемым = протоколируемым
            List<TestCase> tests = new ArrayList<>();
            tests.add(new TestCase2());
            tests.add(new TestCase1());
            tests.add(new TestCase3());
            tests.add(new TestCase4());
            tests.add(new TestCase5());
            tests.add(new TestCase6());
            PrintWriter output = null;
            try {
            // output = new PrintWriter(new FileWriter("log.txt"));
             output = new PrintWriter(new FileWriter("data.log"));
            for(int i = 0; i < tests.size(); i++)
            {
                String strtmp = "test #"+i+" is "+tests.get(i).logResult();
                System.out.println(strtmp);
                output.println(strtmp);
            }
            output.close();
            //Нужно вносить изменения в алгоритм расчета зп developer
        } catch (IOException ex) {
           System.out.println("Не могу создать файл. Нет прав на запись.");
        }
          finally{
           output.close(); 
        }
        
        
    }
    
}
