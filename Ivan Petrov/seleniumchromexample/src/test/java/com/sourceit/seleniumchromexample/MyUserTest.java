/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.sourceit.seleniumchromexample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author T530
 */
public class MyUserTest {
    
    public MyUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @org.junit.Test
    public void testCalcSum() {
        System.out.println("calcSum");
        double a = 1.0;
        double b = 2.0;
        MyUser instance = new MyUser();
        double expResult = 3.0;
        double result = instance.calcSum(a, b);
       assertTrue(result == expResult);
    }
    
}
