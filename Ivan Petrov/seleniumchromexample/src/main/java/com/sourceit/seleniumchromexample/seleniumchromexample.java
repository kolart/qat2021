
package com.sourceit.seleniumchromexample;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.junit.Test;

public class seleniumchromexample {

//Задачи:
    //1 повтор кода с сайта
    //2 получение элемента со своей страницы (от моего MockServer)
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("Hello from seleniumchromexample");
        //указать местоположение WebDriver (путь к exe)
        //shadow - default
        System.setProperty("webdriver.chrome.driver", "C:\\_TMP\\chromedriver.exe");           
         //создать конкретный драйвер
        WebDriver driver = new ChromeDriver(); 
        driver.manage().window().setSize( new Dimension(800, 600));
        driver.manage().window().maximize();
        driver.manage().window().fullscreen();

        
        driver.get("https://www.google.com/");    

        Thread.sleep(5000);  // Let the user actually see something!     

        //WebElement searchBox = driver.findElement(By.name("q"));

        //searchBox.sendKeys("ChromeDriver");     

        //searchBox.submit();    

        Thread.sleep(5000);  // Let the user actually see something!     

        driver.quit();  
        //
    }
    
}
