
package mathshapes;

import java.util.ArrayList;
import java.util.List;


public class MathShapes {


    public static void main(String[] args) {
        //Создать понятия квадрат, прямоугльник. треугольник
        //связть эти понятия наследованием
        
        //в будущем может появиться круг (и точка?)
        //нужно уметь вычислять площадь и периметр фигуры
           
        System.out.println("Hello world from MathShapes");
        //a = 3
        //S = 9, P = 12
        Square sq = new Square(3);
//        double sqArea = sq.calcArea();
//        double sqPerimetr = sq.calcPerimetr();
//        System.out.println("Квадрат "+sq);
//        System.out.println("S = "+sqArea+" P = "+sqPerimetr);
//        //a = 3, b = 4
        //S = 12, P = 14
        Rectangle rec = new Rectangle(3, 4);
//        double recArea = rec.calcArea();
//        double recPerimetr = rec.calcPerimetr();
//        System.out.println("Прямоугольник "+rec);
//        System.out.println("S = "+recArea+" P = "+recPerimetr);
//        //a = 3, b = 4, b = 5
        //S = 6, P = 12
        Triangle trg = new Triangle(3,4,5);
//        double p = trg.calcPerimetr();
//        double s = trg.calcArea();
//        System.out.println("Треугольник "+trg);
//        System.out.println("S = "+s+" P = "+p);
//        
        List<IMathShape> shapes = new ArrayList<>();
        shapes.add(sq);
        shapes.add(rec);
        shapes.add(trg);
        for(int i = 0; i < shapes.size(); i++)
        {
            double P = shapes.get(i).calcPerimetr();
            double S = shapes.get(i).calcArea();
            System.out.println(shapes.get(i).getMyClassName());
            System.out.println(shapes.get(i).toString());
            System.out.println("S = "+S+" P = "+P);
        }
        
    }
    
}
