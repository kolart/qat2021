/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.sourceit.swdexample;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author T530
 */
public class PageObjectTest {
    
    public PageObjectTest() {
    }

   // PageObject mockServer;
    
    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
        
    }
//
//    @org.junit.jupiter.api.AfterAll
//    public static void tearDownClass() throws Exception {
//    }
//
    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
        
    }
//
//    @org.junit.jupiter.api.AfterEach
//    public void tearDown() throws Exception {
//    }
    
 
    @org.junit.jupiter.api.Test
    public void testCanUseElementsInFireFox() {
        System.out.println("can Use Elements in FireFox");
        
//        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
//        WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
//        
//        PageObject mockServer = new PageObject(driver,
//               "https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome",
//               "Тест 1");
        boolean expResult = true;
        boolean result = true;
        assertTrue(result == expResult);
    }
        @org.junit.jupiter.api.Test
    public void testCanUseElementsInChrome() {
        System.out.println("can Use Elements in Chrome");
        
//        System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\chromedriver.exe");
//        WebDriver driver = new ChromeDriver();
//        PageObject mockServer = new PageObject(driver,
//               "https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome",
//               "Тест 1");
        boolean expResult = true;
        boolean result = true;//mockServer.canUseElements();
        assertTrue(result == expResult);
    }

   
    
}
