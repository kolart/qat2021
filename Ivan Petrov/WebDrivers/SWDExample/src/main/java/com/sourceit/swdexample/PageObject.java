package com.sourceit.swdexample;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class PageObject {

    public PageObject(WebDriver driver, String address, String title) {
        this.driver = driver;
        this.address = address;
        this.title = title;
        initPage();
        navigatePage();
    }
    //
    
    //аналог расчет зп, аналог вычисления площади
    //должен знать структуру страницы
    WebElement input;
    public boolean canUseElements()
    {
        input = driver.findElement(By.cssSelector(".myName"));
        if (input == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void navigatePage()
    {
         driver.get(this.address);
    }
    private void initPage()
    {
       driver.manage().window().maximize();
       driver.manage().deleteAllCookies();
       driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    protected WebDriver driver;

    public String getAddress() {
        return this.address;
    }

    public String getTitle() {
        return driver.getTitle();
    }
    protected String address;
    protected String title;
    public void printScreenToFile()
    {
         File screenshort =  ( (TakesScreenshot) driver ).getScreenshotAs(OutputType.FILE);
        // import java.util.Date;
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String fileName = format.format(dateNow)+".png";
        try {
            FileUtils.copyFile(screenshort, new File( "C:\\MyData\\"+fileName  )   );
        } catch (IOException ex) {
            System.out.println("Что-то не так с именем файла");
        } 
    }
}
