package com.sourceit.swdexample;
//SWD
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
//
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
//работа с файлами
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

//
public class SWDExample {

    /*
    Общий класс - родитель всех страниц
    +проверка какая страница открыта адрес
    +заголовк надпись на страницы
    +printscreen
    +доступны ли к заполнению структурные компоненты
    
    */
    /*
    Проверить вход при использовании
    FireFox
    Chrom
    используя только JUnit
    */
    /*
    Проверить вход при использовании
    FireFox
    Chrom
    используя только Иерархию Классов
    */
    public static void main(String[] args) {
       System.out.println("Hello from SWDExample");
       //
       System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
       WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
       //
       driver.manage().window().maximize();
       driver.manage().deleteAllCookies();
       driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
       driver.get("https://www.browserstack.com/users/sign_in");
       System.out.println("Заголовок 1 "+driver.getTitle());
       System.out.println("Адрес 1 "+driver.getCurrentUrl()); 
       //
       driver.findElement(By.id("accept-cookie-notification")).click();
       System.out.println("Cookies - pass");
       //
        WebElement username = driver.findElement(By.id("user_email_login"));// user[login]  input-lg text user_email_ajax
// WebElement username=driver.findElement(By.name("user[login]"));
// WebElement username = driver.findElement(By.cssSelector(".input-lg text user_email_ajax"));
        System.out.println("username is " + username.toString());
        WebElement password = driver.findElement(By.id("user_password"));
        System.out.println("password is " + password.toString());
        WebElement login = driver.findElement(By.name("commit"));
        System.out.println("login is " + login.toString());
        username.sendKeys("artem.konstantinovich@ukr.net");
        password.sendKeys("Password123_");
        System.out.println("login data pass");
        login.click();

       //
         try {
            //  button[type='submit']
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.print("Tread exception");
        }
       
       
       driver.quit();
       //
    }
  
    
    public static void main_minfin(String[] args) {
       System.out.println("Hello from SWDExample 2021-12-17");
       //
       System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
       WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
       //в конструктор родителя - в PageObject

       //констурктор LoginPage
       LoginPage loginPage = new LoginPage(driver,
                             "https://developers.minfin.com.ua/login/",
                              "LOGIN MINFIN");
       System.out.println("Заголовок 1 "+loginPage.getTitle());
       System.out.println("Адрес 1 "+loginPage.getAddress()); 
       loginPage.printScreenToFile();
       loginPage.navigateToLoginPage();
       //
       if (loginPage.canUseElements())
       {
          loginPage.printScreenToFile(); 
          loginPage.login();
          loginPage.printScreenToFile(); 
          //
  
          //
          String actualAdress = loginPage.getAddress();
          String expectedUrl = "https://developers.minfin.com.ua/";
          System.out.println("actualAdress is "+actualAdress);
          System.out.println("expectedUrl is "+expectedUrl);
       }
        //
        // username.sendKeys("artem.konstantinovich@ukr.net");
        // password.sendKeys("Password123_" + Keys.ENTER);
        // login.click();
        // String expectedUrl = "https://developers.minfin.com.ua/";

       //
         try {
            //  button[type='submit']
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.print("Tread exception");
        }
        
       driver.quit();
       //
    }
    
}
