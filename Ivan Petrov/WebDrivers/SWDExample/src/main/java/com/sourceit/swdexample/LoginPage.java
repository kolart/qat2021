package com.sourceit.swdexample;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage extends PageObject {
    
    private String address;
    private String title;
    private WebDriver driver;
    //
    private WebElement username;
    private WebElement password;
    private WebElement login;
    public LoginPage(WebDriver driver, String address, String title) {
        super(driver, address, title);
        //только для варианта 2
        this.driver = driver;
        this.address = address;
        this.title = title;
    }
    public LoginPage login()
    {
         username.sendKeys("artem.konstantinovich@ukr.net");
         password.sendKeys("Password123_" + Keys.ENTER);
         login.click();
                  try {
            //  button[type='submit']
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.print("Tread exception");
        }
         return this;
    }
    //вариант 3 - обобщенный вариант 1 = Вводиv ResultPage
    // и тогда метод для теста возвращает ResultPage
    @Override
    public boolean canUseElements() {
        this.username = driver.findElement(By.name("login"));
        if (this.username==null)
            return false;
        this.password = driver.findElement(By.name("password"));
        if (this.password==null)
            return false;
        this.login = driver.findElement(By.cssSelector("button[type='submit']"));// //button[@type='submit']
        if (this.password == null)
            return false;
       return true;
    }

    
    public void navigateToLoginPage() {
       WebElement link = driver.findElement(By.cssSelector("a[href='/login/']"));
       if (link!=null)
       {
          System.out.println("Have link "+link);
          link.click();
       }
       
    }
    
    
}
