
package junitexample3;

//ChildrenTestTasks
public class TestTask {
    //
    public static boolean assertExpression(boolean result, 
            boolean expected)
    {
        if (result == expected)
        {
            System.out.println("log: assert expression is true");
            return true;
        }
        else
        {
            System.out.print("log: assert expression is false");
            return false;
        }
    }
    //
    private Children ch1;
    public void doSetUpClass()
    {
        System.out.println("0. Один раз");
        ch1 = new Children("Ivan Petrov");
    }
    public void doSetUp()
    {
        System.out.println("1.Каждый раз. Перед методом doTest");
    }
    public void doTearDownClass()
    {
        System.out.println("Один раз. В самом конце");
    }
    public void doTearDown()
    {
        System.out.println("2.Каждый раз. После методом doTest");
        //закрытие соединени с базой данных
        //закрытие файла
        //удаление соединения с сайтом
    }
    //является ли класс наследником
    //слов test описание теста - параметры - ожидаемое значение - результат
    //test_descriptiontest_params_expected_result
    //test_isPerentCorrect_Children_Parent_true_pass
    public boolean test_haveCorrectParent_true_pass()
    {
        //в JUnit метод вохвращает void
        //но заканчивается командой assert...
        boolean result = false;
        //
        
        if ( ch1 instanceof Parent  )
        {
          //System.out.println("Класс Children наследник Parent");
          //System.out.println(ch1.getClass() + " is a " + Parent.class);
          result = true;
        }
        //
        return result;
    }
    //работает ли метод sayHello
    //изменить имя
    //написать проверку
    public void test_callSayHello_true_pass()
    {
        //в JUnit метод вохвращает void
        //но заканчивается командой assert...
        boolean result = false;
        //
        result = this.ch1.sayHello().equalsIgnoreCase("Hello, my name is Ivan Petrov");
        //
        boolean expected = true;
        assertExpression(result, expected);
    }
    //
    public void test_IsTaxMinSalaryCorrect_true_pass()
    {
        boolean result = false;
        double minSalary = 6000;
        Tax tax = TaxCalc.calcTax(minSalary);
        if(
             tax.pension == minSalary * 0.22
          && tax.pn == minSalary * 0.18
          && tax.prof == minSalary * 0.04      
           )
        {
            result = true;
        }
        else
        {
            result = false;
        }
        boolean expected = true;
        TestTask.assertExpression(result, expected);
    }
    
}
