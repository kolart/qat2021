
package junitexample3;


public class Parent {
    
    private String name;
    public Parent(String name)
    {
        this.name = name;
    }
    public String sayHello(){
        return "Hello, my name is "+name;
    }
    
}
