
package junitexample3;


public class JUnitExample3 {


    public static void main(String[] args) {
      //тест иерархии наличие extends
      //и пример сохания автотестов - модульных тестов
      //эталон теста
      //логику эталона и механизмы его запуска повторим руками
      
//      Children ch1 = new Children("Ivan Petrov");
//      if ( ch1 instanceof Parent  )
//      {
//          System.out.println("Класс Children наследник Parent");
//          System.out.println(ch1.getClass() + " is a " + Parent.class);
//      }
       //используя механизм рефлексии среда запуска тестов
       //прсмтривает класс тестов (все классы из файлов Test.java, в имени которых есть Test)
       //изет все методы которые отмечены атрибутами
       //запускает их друг за другом
       TestTask test1 = new TestTask();
       test1.doSetUpClass();
       test1.doSetUp();
       test1.test_haveCorrectParent_true_pass();
       test1.doTearDown();
       test1.doSetUp();
       test1.test_callSayHello_true_pass();
       test1.doTearDown();
       test1.doSetUp();
       test1.test_IsTaxMinSalaryCorrect_true_pass();
       test1.doTearDown();
       test1.doTearDownClass();
      
      
    }
    
}
