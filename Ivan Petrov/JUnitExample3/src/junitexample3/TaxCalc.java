
package junitexample3;


public class TaxCalc {
    public static Tax calcTax(double salary)
    {
        Tax tax = new Tax();
        tax.pension = 0.22 * salary;
        tax.pn = 0.18 * salary;
        tax.prof = 0.04 * salary;
        
        return tax;
    }
}
