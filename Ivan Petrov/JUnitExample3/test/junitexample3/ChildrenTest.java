package junitexample3;


import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ChildrenTest {
    
    public ChildrenTest() {
    }
    
    @org.junit.BeforeClass
    public static void setUpClass() {
        System.out.println("call setUpClass()");
    }
    
    @org.junit.AfterClass
    public static void tearDownClass() {
        System.out.println("call tearDownClass()");
    }
    
    @org.junit.Before
    public void setUp() {
        System.out.println("call setUp()");
    }
    
    //@AfterEach
    @org.junit.After
    public void tearDown() {
        System.out.println("call tearDown()");
    }

    //@Test
    @org.junit.Test
    public void testSomeMethod() {
        System.out.println("call testSomeMethod()");
        // fail("The test case is a prototype.");
        boolean result = true;
        boolean expected = true;
        // assertEquals(Double.NaN, Double.NaN);
        //assertEquals(result, expected);
        assertTrue(result);
    }
    
}
