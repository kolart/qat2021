package com.mycompany.jsonparsermavenexample;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import myobjects.BeanA;
import myobjects.MyBean;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class JSONParserMavenExample {


    public static void main(String[] args) throws UnsupportedOperationException, IOException {
        System.out.println("Hello from JSONParserMavenExample");
        //клиентская часть
        //json от сервера
        String json = "{bool:true,integer:1,string:\"json\"}"; 
        //термин обычный класс на java = bean class
        JSONObject jsonObject = JSONObject.fromObject( json );  
        //класс BeanA содержит поля имена, которых совпадают с полями json
        //значит можно этот класс создать из json ответа сервера
        BeanA bean = (BeanA) JSONObject.toBean( jsonObject, BeanA.class );
        System.out.println("JSON serialize test 1");
        System.out.println(bean);
        //а вот команды для серверной
        //класс MyBean сохраняем в json
        System.out.println("JSON serialize test 2");
        JSONObject jsonObject2 = JSONObject.fromObject( new MyBean() );  
        System.out.println( jsonObject2.toString() ); 
        
        //
        String result = "";
      String urladr = "https://0aff146f-63f6-4ea5-b05b-0857922e27cc.mock.pstmn.io";
      
      result = getResponseToJson(urladr, result);
        
      System.out.println("input data:");
      System.out.println(result);
        
        //
    }
    //
      public static String getResponseToJson(String urladr, String result) throws UnsupportedOperationException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(urladr);
        System.out.println("Request Type: "+httpget.getMethod());
        //Executing the Get request
        HttpResponse httpresponse = httpclient.execute(httpget);
        Scanner sc = new Scanner(httpresponse.getEntity().getContent());
        //Printing the status line
        System.out.println(httpresponse.getStatusLine());
        while(sc.hasNext()) {
            String line = sc.nextLine();
            System.out.println(line);
            result+=line;
        }
        return result;
    }

    //
}
