
package stringsexample3;

import strings.MyStrings;

/**
 *
 * @author T530
 */
public class StringsExample3 {

    
    public static void main(String[] args) {
        
        //вывести буквы, которые стоят на
        //нечетных местах
                    //0123456789
        String str = "Аргентина манит негра";
                    //123456789
                    //А г н
        String res = MyStrings.getEventSybmols(str);
        System.out.println(res);
        //опеределить, является ли строка палиндромом
        
        // str = "Казак";
        str = "Аргентина манит негра";
                //Алгоритм 1
        //первая буква совпадает с последней
        //вторая с предпоследней
        // 0 = n - 1 - 0
        // 1 = n - 1 - 1
        // 2 = n - 1 - 2
        // 3 = n - 1 - 3
        // i = n - 1 - i, i = 0,1,2,3,..., n / 2
        boolean state = MyStrings.isPalyndrom(str);
        if (state == true)
        {
            System.out.println("слово "+str+" палиндром");
        }
        else
        {
            System.out.println("слово "+str+" НЕпалиндром");
        }
        
    }
    
}
