/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package junitsquaretest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author T530
 */
public class SquareTest {
    Square instance = null;
    public SquareTest() {
    }
    
    @org.junit.BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @org.junit.Before
    public void setUp() {
        this.instance = new Square(3);
    }
    
    @AfterEach
    public void tearDown() {
    }

    @org.junit.Test
    public void testCalcArea() {
        System.out.println("calcArea");
        double expResult = 9.0;
        double result = instance.calcArea();
        assertTrue(expResult == result);
    }

    @org.junit.Test
    public void testCalcP() {
        System.out.println("calcP");
        Square instance = new Square(3);
        double expResult = 12.0;
        double result = instance.calcP();
        assertTrue(expResult == result);
    }

    // @org.junit.Ignore
    @org.junit.Test
    public void testGetA() {
        testSetA();
        System.out.println("getA");
        double expResult = 0.0;
        double result = instance.getA();
        assertTrue(expResult == result);
    }

    @org.junit.Ignore
    public void testSetA() {
        System.out.println("setA");
        double a = 0.0;
        instance.setA(a);
    }
    
}
