
package javareflectiontest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaReflectionTest {

 
    public static void main(String[] args) {
      
        
        System.out.println("Примеры работы с классом");
        //Список методов класса
        String className = "javareflectiontest.User";
        
        Class clazz;
        try {
            clazz = Class.forName(className);//это аналог констуктора
            Class superClasses =  clazz.getSuperclass();
            System.out.println("у класса есть родитель "+superClasses);
            System.out.println("у класса есть аннотация "+clazz.isAnnotation());
             //import java.lang.reflect.Constructor;            
            Method[] methods = clazz.getMethods();
            System.out.println("Список методов класса:" + className);
            for(Method m: methods)
            {
                 System.out.println(m.toString());
            }



//Class clazz = MyUser.class;
//        Class clazz;
//        Method method;
//        String methodName = "getEmail";
//        //Как извлечь реализацию
//        //только одного метода из класса
//        //и вызвать ее
//         
//        method = user.getClass().getMethod(methodName);
//        method.invoke(user);//this
//
//        method = user.getClass().getMethod("setEmail", 
//                    String.class);
//        method.invoke(user, "tester2@gmail.com");
//        
        } catch (ClassNotFoundException ex) {
            System.out.println("Класс с именем " + className + " не найден в jar файле");
        }
        
    }
    
}
