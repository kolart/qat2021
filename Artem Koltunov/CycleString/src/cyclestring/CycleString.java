package cyclestring;

import java.util.Scanner;


public class CycleString {


    public static void main(String[] args) {
        //+вывести цифры числа в обратном порядке
        //324 --> 423
        //+ определить, количество цифр в числе
        //+определить, есть ли в строке символ 7
        //посчитать количество пробелов
        int x = 324;
                     //0 1 2
        String str1 = "324";
        //int y = Integer.parseInt(str1);
        System.out.println("строка = "+str1);
        //java, c#, javascruipt, typescript, c++
        //нумерация начинается от НУЛЯ
        System.out.println("символ номер 2 в ней это "+str1.charAt(1));
        int n = str1.length();//количество символов в строке (штуки)
        // номер последней буквы, это количество символов минус 1
        System.out.println("Перевернутая строка:");
        for(int i = n - 1; i >= 0; i--)
        {
            //System.out.println(str1.charAt(i));
            System.out.print(str1.charAt(i));
        }
        boolean state = false;// случай да или нет, то лучше boolean
        char symbol = ' ';
        for(int i = 0; i < str1.length(); i++ )
        {
            if (str1.charAt(i) == symbol)// одинарные кавычки означают  одну букву - символ
            {
                System.out.println("Символ 7 входит");
                state = true;
                break;
            }
        }
        if (state == false)
        {
            System.out.println("Символ пробел НЕ входит");
        }
        System.out.print("Введите вторую  (123)");
        String str2 = "123";
        Scanner input = new Scanner (System.in);
        String str3 = input.nextLine();// "123";
        if (str2==str3)
        {
            System.out.println("strings are equal");
        }
        else
        {
            System.out.println("strings are not equal");
        }
        if (str2 == "123")
        {
            System.out.println("str2 equals 1234");
        }
    }
    
}
