package cyclesexample;
import java.util.Scanner;

public class CyclesExample {

    
    public static void main(String[] args) {
        // Вводи цлое положительное число. Например, 3
        //Выводим перевод дюймов, в сантиметры
        // 1 - 2.54
        // 2 - 5.08
        // 3 - 7.62
        int count = 3;
        //1
        do
        {
           System.out.println("Введите целое положительное число");
           Scanner input = new Scanner(System.in);
           count = input.nextInt(); 
        }while (count <= 0);//2
        
            //1       //2           //4
        for(int i = 1;  i <= count ; i = i + 1 )
        {
         //3 = то, что нужно повторять
          int inch = i;
          double cm = MyFunctions.incheToCm(inch);
          System.out.println("дюймы = "+inch+" см = "+cm);    
        }
        
        
    }
    
}
