
package calcformula8;

import java.util.Scanner;


public class CalcFormula8 {


    public static void main(String[] args) {
        System.out.println("Вычисление площади треугольника по известному радиусу вписанной окружности и полупериметру  ");
        System.out.println();
        System.out.println("Введите радиус вписанной окружности ");
        Scanner myInput1 = new Scanner ( System.in  );
        double r = myInput1.nextDouble();
        System.out.println("Введите периметр ");
        Scanner myInput2 = new Scanner ( System.in  );
        double P = myInput2.nextDouble();
        double S = P*r/2;
        System.out.println("Площадь треугольника равна " + S);
    }
    
}
