
package calcformula5;

import java.util.Scanner;

public class CalcFormula5 {


    public static void main(String[] args) {
        System.out.println("Вычисление площади ромба, если известен сторона и острый угол ");
        System.out.println();
        System.out.println("Введите сторону ");
        Scanner myInput1 = new Scanner ( System.in  );
        double a = myInput1.nextDouble();
        System.out.println("Введите острый угол ромба в градусах ");
        Scanner myInput2 = new Scanner ( System.in  );
        double alfa = myInput2.nextDouble();
        double S = Math.pow(a, 2)* Math.sin(Math.toRadians(alfa));
        System.out.println("Площадь ромба равна " + S);
    }
    
}
