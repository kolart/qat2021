
package calcif3;

import java.util.Scanner;

public class CalcIF3 {


    public static void main(String[] args) {
        System.out.println("Проверка является ли вводимое число четным");
        System.out.println();
        System.out.println("Введите число");
        Scanner myInput1 = new Scanner ( System.in  );
        double A = myInput1.nextDouble();
        if(A==0)
            {
                System.out.println("Вы ввели ноль");
                return;
            }
        if(A%2==0)
            {
                System.out.println("Число является четным");
            }
            else
            {
                System.out.println("Число не является четным");
            }
    }
}
