
package calcformula3;

import java.util.Scanner;

public class CalcFormula3 {


    public static void main(String[] args) {
        System.out.println("Вычисление площади окружности ");
        System.out.println();
        System.out.println("Введите радиус окружности ");
        Scanner myInput1 = new Scanner ( System.in  );
        double r = myInput1.nextDouble();
        double So = Math.PI*Math.pow(r, 2);
        System.out.println("Площадь окружности равна " + So);
    }
    
}
