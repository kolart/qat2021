
package calcformula6;

import java.util.Scanner;

public class CalcFormula6 {


    public static void main(String[] args) {
        System.out.println("Вычисление площади ромба, по его диагоналям ");
        System.out.println();
        System.out.println("Введите первую диагошаль ");
        Scanner myInput1 = new Scanner ( System.in  );
        double d1 = myInput1.nextDouble();
        System.out.println("Введите вторую диагошаль ");
        Scanner myInput2 = new Scanner ( System.in  );
        double d2 = myInput2.nextDouble();
        double S = d1*d2/2;
        System.out.println("Площадь ромба равна " + S);
    }
    
}
