
package calcformula2;

import java.util.Scanner;

public class CalcFormula2 {


    public static void main(String[] args) {
        System.out.println("Вычисление объема пирамиды ");
        System.out.println();
        System.out.println("Введите площадь основания пирамиды ");
        Scanner myInput1 = new Scanner ( System.in  );
        double So = myInput1.nextDouble();
        System.out.println("Введите высоту пирамиды ");
        Scanner myInput2 = new Scanner ( System.in  );
        double H = myInput2.nextDouble();
        double V = So*H/3;
        System.out.println("Объем пирамиды равен " + V);
    }
    
}
